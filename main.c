#include "9cc.h"

int main(int argc, char **argv)
{
  if (argc != 2)
    error("引数の個数が正しくありません");

  // トークナイズしてパースする
  char *input_code = argv[1];
  Token *token = tokenize(input_code);
  Node *node = parse(token);
  codegen(node);
  return 0;
}
